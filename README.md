# node-angular-mongoDB-auth
basic authentication project using Angular5, Saas, Node Typescript, MongoDB

## Prerequisites: 
   * 1. running mongoDB and change the DB_URI on src/config.ts to your DB uri  
   * 2. make sure you have node install on your machine.

## to start the project run in cmd:
in Project_Directory: 
   * 1. npm install  
   * 2. npm start
   
in Project_Directory/angular-src:
   * 1. npm install  
   * 2. ng serve
   

