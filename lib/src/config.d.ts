export declare const CLIENT_URL = "http://localhost:4200";
export declare const DB_URI = "mongodb://localhost:27017/Sonder";
export declare const TOKEN_SECRET = "SonderSecret";
export declare const CORS_OPTIONS: {
    origin: string;
    optionsSuccessStatus: number;
    allowHeaders: string;
    methods: string;
    credentials: boolean;
};
